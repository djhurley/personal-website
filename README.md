Steps to setup development environment:

1. Download and install Node.js from the following website:
**https://nodejs.org/en/**

2. Execute the following command to install bower and other tools needed:  
***sudo npm install -g bower wiredep connect serve-static***

3. Execute the following command to download project dependencies:  
***bower install***